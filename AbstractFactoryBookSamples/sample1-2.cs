﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // doğrudan fabrika sınıflarını kullanalım
    //        BaseReportFactory excelReport = new ExcelReportFactory();
    //        excelReport.GetConnector();
    //        excelReport.GetFormatter();
    //        excelReport.GetWriter();

    //        // report manager ? report processor ?
    //        ReportProcessor pro = new ReportProcessor();
    //        pro.Generate(new ExcelReportFactory(), "");
    //    }
    //}

    //// Abstract Products
    //public abstract class BaseReportConnector { }
    //public abstract class BaseReportFormatter { }
    //public abstract class BaseReportWriter { }

    //// Concrete Product for Excel Report
    //public class ExcelReportConnector : BaseReportConnector { }
    //public class ExcelReportFormatter : BaseReportFormatter { }
    //public class ExcelReportWriter : BaseReportWriter { }

    //// Abstract Factory
    //public abstract class BaseReportFactory
    //{
    //    public abstract BaseReportConnector GetConnector();
    //    public abstract BaseReportFormatter GetFormatter();
    //    public abstract BaseReportWriter GetWriter();
    //}

    //// Concrete Factory
    //public class ExcelReportFactory
    //    : BaseReportFactory
    //{
    //    public override BaseReportConnector GetConnector()
    //    {
    //        // ? Singleton
    //        return new ExcelReportConnector();
    //    }

    //    public override BaseReportFormatter GetFormatter()
    //    {
    //        return new ExcelReportFormatter();
    //    }

    //    public override BaseReportWriter GetWriter()
    //    {
    //        return new ExcelReportWriter();
    //    }
    //}

    //// istemci kod blogu
    //public class ReportProcessor
    //{
    //    public ReportProcessor() { }

    //    public ReportProcessor(BaseReportFactory factory, object data)
    //    {
    //        Generate(factory, data);
    //    }

    //    public void Generate(BaseReportFactory factory, object data)
    //    {
    //        BaseReportConnector conn = factory.GetConnector();
    //        BaseReportFormatter formatter = factory.GetFormatter();
    //        BaseReportWriter writer = factory.GetWriter();
    //    }
    //}
}
