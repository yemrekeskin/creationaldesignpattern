﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryBookSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogFactory factory = new XmlLogFactory();
            ILogConnect conn = factory.CreateLogConnect();
            ILogConfig conf = factory.CreateLogConfig();
            ILogger logger = factory.CreateLogger();

            // ?
            XmlLogConnect xmlConn = new XmlLogConnect();
            
        }
    }


    public interface ILogConfig { }
    public interface ILogConnect { }
    public interface ILogger { }

    public class XmlLogConfig : ILogConfig { }
    public class XmlLogConnect : ILogConnect { }
    public class XmlLogger : ILogger { }

    public class DbLogConfig : ILogConfig { }
    public class DbLogConnect : ILogConnect { } 
    public class DbLogger : ILogger { }

    public interface ILogFactory
    {
        ILogConfig CreateLogConfig();
        ILogConnect CreateLogConnect();
        ILogger CreateLogger();   
    }

    public class XmlLogFactory
        : ILogFactory
    {
        public ILogConfig CreateLogConfig()
        {
            return new XmlLogConfig();
        }

        public ILogConnect CreateLogConnect()
        {
            return new XmlLogConnect();
        }

        public ILogger CreateLogger()
        {
            return new XmlLogger();
        }
    }

    public class DbLogFactory
        : ILogFactory
    {
        public ILogConfig CreateLogConfig()
        {
            return new DbLogConfig();
        }

        public ILogConnect CreateLogConnect()
        {
            return new DbLogConnect();
        }

        public ILogger CreateLogger()
        {
            return new DbLogger();
        }
    }



}
