﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Product p1 = new Product(new XmlProductProvider(), 32, "Car");
            Product p2 = new Product(new XmlProductProvider(), 36, "Elbise");
        }
    }

    public interface IProductProvider { }

    public class XmlProductProvider 
        : IProductProvider
    {

    }

    public class Product
    {
        public Product(IProductProvider provider, short categoryCode, string productName)
        {
            this.CategoryCode = categoryCode;
            this.ProductName = productName;
        }

        public int ProductId { get; set; }
        public short CategoryCode { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
    }





    public class Course
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }

    }
}




