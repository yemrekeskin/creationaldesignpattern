﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        var producta = new ProductA();
    //        var producta1 = producta.Clone();

    //        var productb = new ProductB();
    //        var productb1 = productb.Clone();

    //        Console.ReadLine();
    //    }
    //}

    //public class ProductA
    //    : ICloneable
    //{
    //    public object Clone()
    //    {
    //        return MemberwiseClone() as ProductA;
    //    }
    //}

    //public class ProductB
    //{
    //    public ProductB Clone()
    //    {
    //        return MemberwiseClone() as ProductB;
    //    }
    //}


    // 2
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Product product = new Product();
    //        Product product1 = product.Clone() as Product;


    //        Console.ReadLine();
    //    }
    //}

    //public abstract class IPrototype<T>
    //{
    //    public T Clone()
    //    {
    //        return (T)this.MemberwiseClone();
    //    }
    //}

    //public class Product
    //    : IPrototype<Product>
    //{

    //}

    // 3
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Product product = new Product();
    //        Product productClone = product.Clone();

    //        Console.ReadLine();
    //    }
    //}

    //public interface IPrototype<T>
    //{
    //    T Clone();
    //}

    //public class Product
    //    : IPrototype<Product>
    //{


    //    public Product Clone()
    //    {
    //        // serilization
    //        MemoryStream stream = new MemoryStream();
    //        BinaryFormatter formatter = new BinaryFormatter();
    //        formatter.Serialize(stream, this);
    //        stream.Seek(0, SeekOrigin.Begin);

    //        // deserilization
    //        T copy = (T)formatter.Deserialize(stream);
    //        stream.Close();

    //        return copy;
    //    }
    //}

    // 4
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Product product = new Product();
    //        product.MyProperty = "test";

    //        PrototypeManager manager = new PrototypeManager();
    //        manager.Add(0, product);

    //        Product productClone = manager.Get(0) as Product;
    //        productClone.MyProperty = "newTest";

    //        Console.WriteLine(product.MyProperty);
    //        Console.WriteLine(productClone.MyProperty);

    //        Console.ReadLine();
    //    }
    //}

    //public abstract class BasePrototype<T>
    //{
    //    public T Clone()
    //    {
    //        return (T)MemberwiseClone();
    //    }
    //}

    //public class Product
    //    : BasePrototype<Product>
    //{
    //    public string MyProperty { get; set; }
    //}

    //class PrototypeManager
    //    : BasePrototype<Product>
    //{
    //    private Dictionary<int, BasePrototype<Product>> list = new Dictionary<int, BasePrototype<Product>>();

    //    public void Add(int index, BasePrototype<Product> p)
    //    {
    //        list.Add(index, p);
    //    }

    //    public BasePrototype<Product> Get(int index)
    //    {
    //        return list[index].Clone();
    //    }
    //}

    // 6

}
