﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultitonBookSamples
{
    // sample 1
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // Multiton o1 = new Multiton();
    //        Multiton o1 = Multiton.GetInstance("N");
    //        Multiton o2 = Multiton.GetInstance("M");
    //        Multiton o3 = Multiton.GetInstance("N");

    //        Console.WriteLine(o1.quid);
    //        Console.WriteLine(o2.quid);
    //        Console.WriteLine(o3.quid);

    //        Console.ReadLine();
    //    }
    //}

    //public class Multiton
    //{
    //    public Guid quid { get; set; }

    //    static Dictionary<string, Multiton> _instances = new Dictionary<string, Multiton>();
    //    static object _lock = new object();

    //    private Multiton()
    //    {
    //        // private ctor
    //        quid = Guid.NewGuid();
    //    }

    //    public static Multiton GetInstance(string key)
    //    {
    //        lock (_lock)
    //        {
    //            if (!_instances.ContainsKey(key))
    //                _instances.Add(key, new Multiton());
    //        }
    //        return _instances[key];
    //    }
    //}

    // sample 2
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // Multiton o1 = new Multiton();
    //        Multiton o1 = Multiton.GetInstance("N");
    //        Multiton o2 = Multiton.GetInstance("M");
    //        Multiton o3 = Multiton.GetInstance("N");

    //        Console.WriteLine(o1.quid);
    //        Console.WriteLine(o2.quid);
    //        Console.WriteLine(o3.quid);

    //        Console.ReadLine();
    //    }
    //}

    //public class Multiton
    //{
    //    public Guid quid { get; set; }

    //    static ConcurrentDictionary<string, Multiton> _instances = new ConcurrentDictionary<string, Multiton>();

    //    private Multiton()
    //    {
    //        // private ctor
    //        quid = Guid.NewGuid();
    //    }

    //    public static Multiton GetInstance(string key)
    //    {
    //        return _instances.GetOrAdd(key, (k) => new Multiton());
    //    }
    //}

    // sample 3

    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        ScriptEngine JqueryEngine = ScriptEngine.Get("Jquery");
    //        ScriptEngine JavascriptEngine = ScriptEngine.Get("Javascript");

    //        Console.ReadLine();
    //    }
    //}

    //public class ScriptEngine
    //{
    //    private static readonly ConcurrentDictionary<object, Lazy<ScriptEngine>> _instances = new ConcurrentDictionary<object, Lazy<ScriptEngine>>();

    //    private ScriptEngine()
    //    {
    //    }

    //    public static ScriptEngine Get(string key)
    //    {
    //        Lazy<ScriptEngine> instance = _instances.GetOrAdd(key, k => new Lazy<ScriptEngine>(() => new ScriptEngine()));
    //        return instance.Value;
    //    }
    //}
}
