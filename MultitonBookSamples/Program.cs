﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultitonBookSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            ScriptEngine JqueryEngine = ScriptEngine.Get("Jquery");
            ScriptEngine JavascriptEngine = ScriptEngine.Get("Javascript");
            
            Console.ReadLine();
        }
    }

    public class ScriptEngine
    {
        private static readonly ConcurrentDictionary<object, Lazy<ScriptEngine>> _instances = new ConcurrentDictionary<object, Lazy<ScriptEngine>>();

        private ScriptEngine()
        {
        }

        public static ScriptEngine Get(string key)
        {
            Lazy<ScriptEngine> instance = _instances.GetOrAdd(key, k => new Lazy<ScriptEngine>(() => new ScriptEngine()));
            return instance.Value;
        }
    }



}
