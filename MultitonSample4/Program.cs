﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultitonSample4
{
    class Program
    {
        static void Main(string[] args)
        {
            ScriptEngine engine = ScriptEngineManager.Get("JS");
            ScriptEngine en = new ScriptEngine();

            // generic multiton
            var jsEngine = ScriptEngine_New.GetInstance("JS");
            var aspxEngine = ScriptEngine_New.GetInstance("ASPX");
            ScriptEngine_New aspEngine = ScriptEngine_New.GetInstance("JS");
            
            Console.ReadLine();
        }
    }

    public class ScriptEngine
    {
        // ?? private ctor
    }

    public class ScriptEngineManager
    {
        private static readonly ConcurrentDictionary<object, Lazy<ScriptEngine>> _instances = new ConcurrentDictionary<object, Lazy<ScriptEngine>>();

        private ScriptEngineManager()
        {

        }

        public static ScriptEngine Get(string key)
        {
            Lazy<ScriptEngine> instance = _instances.GetOrAdd(key, k => new Lazy<ScriptEngine>(() => new ScriptEngine()));
            return instance.Value;
        }
    }

    // generic multiton
    public class Multiton<T>
        where T : class, new()
    {
        private static readonly ConcurrentDictionary<object, Lazy<T>> _instances = new ConcurrentDictionary<object, Lazy<T>>();

        public static T GetInstance(object key)
        {
            Lazy<T> instance = _instances.GetOrAdd(key, k => new Lazy<T>(() => new T()));
            return instance.Value;
        }

        protected Multiton()
        {
        }
    }

    public class ScriptEngine_New
        : Multiton<ScriptEngine_New>
    {

    }
}
