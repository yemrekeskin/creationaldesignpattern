﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {

    //        var maker = new ProductMaker(new ProductrBuilder());
    //        var product = maker.Get();

    //        Console.ReadLine();
    //    }
    //}

    //// product
    //public class Product
    //{
    //    public string Property1 { get; set; }
    //    public int Property2 { get; set; }
    //    public decimal Property3 { get; set; }
    //    public SubProduct Property4 { get; set; }
    //    public ProductType Property5 { get; set; }
    //}

    //public class SubProduct
    //{
    //    public bool Property0;
    //    public string Property1;
    //}

    //public enum ProductType { Type0, Type1 }

    //// abstract builder
    //public interface IProductBuilder
    //{
    //    IProductBuilder AddProperty1(string property1);
    //    IProductBuilder AddProperty2(int property2);
    //    IProductBuilder AddProperty3(decimal property3);
    //    IProductBuilder AddProperty4(SubProduct property4);

    //    Product GetProduct();
    //}

    //// concrete builder
    //public class ProductrBuilder
    //    : IProductBuilder
    //{
    //    private Product product;

    //    public IProductBuilder AddProperty1(string property1)
    //    {
    //        product.Property1 = property1;
    //        return this;
    //    }

    //    public IProductBuilder AddProperty2(int property2)
    //    {
    //        product.Property2 = property2;
    //        return this;
    //    }

    //    public IProductBuilder AddProperty3(decimal property3)
    //    {
    //        product.Property3 = property3;
    //        return this;
    //    }

    //    public IProductBuilder AddProperty4(SubProduct property4)
    //    {
    //        product.Property4 = property4;
    //        return this;
    //    }

    //    public Product GetProduct()
    //    {
    //        return product;
    //    }
    //}

    //// directory or maker
    //public class ProductMaker
    //{
    //    private IProductBuilder Builder = null;
    //    public ProductMaker(IProductBuilder builder)
    //    {
    //        this.Builder = builder;
    //    }

    //    public Product Get()
    //    {
    //        return Builder.AddProperty1("property1")
    //                      .AddProperty2(2)
    //                      .AddProperty3(3)
    //                      .AddProperty4(new SubProduct())
    //                      .GetProduct();
    //    }

    //}
}
