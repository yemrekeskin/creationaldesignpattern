﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuilderBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        var product = new Product("property1", 2, 3, new SubProduct(), ProductType.Type0);

    //        Console.ReadLine();
    //    }
    //}

    //// product
    //public class Product
    //{
    //    public string Property1 { get; set; }
    //    public int Property2 { get; set; }
    //    public decimal Property3 { get; set; }
    //    public SubProduct Property4 { get; set; }
    //    public ProductType Property5 { get; set; }

    //    public Product(string property1, int property2, decimal property3, SubProduct property4, ProductType property5)
    //    {
    //        Property1 = property1;
    //        Property2 = property2;
    //        Property3 = property3;
    //        Property4 = property4;
    //        Property5 = property5;
    //    }

    //    public override string ToString()
    //    {
    //        // display
    //        return base.ToString();
    //    }

    //}

    //public class SubProduct
    //{
    //    public bool Property0;
    //    public string Property1;
    //}

    //public enum ProductType { Type0, Type1 }
}
