﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuilderBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Directory dir = new Directory();
    //        Product product = dir.Construct(new ConcreteBuilder());


    //        Console.ReadLine();
    //    }
    //}

    //// product
    //public class Product
    //{
    //    public string Property1 { get; set; }
    //    public int Property2 { get; set; }
    //}

    //// abstract builder
    //public abstract class BaseBuilder
    //{
    //    public abstract void BuildPartA();
    //    public abstract void BuildPartB();

    //    public abstract Product GetProduct();
    //}

    //// concrete builder
    //public class ConcreteBuilder
    //    : BaseBuilder
    //{
    //    private Product product = new Product();

    //    public ConcreteBuilder()
    //    {

    //    }

    //    public override void BuildPartA()
    //    {
    //        product.Property1 = "newValue";
    //    }

    //    public override void BuildPartB()
    //    {
    //        product.Property2 = 12;
    //    }

    //    public override Product GetProduct()
    //    {
    //        return product;
    //    }
    //}

    //public class Directory
    //{
    //    public Product Construct(BaseBuilder builder)
    //    {
    //        builder.BuildPartA();
    //        builder.BuildPartB();

    //        return builder.GetProduct();
    //    }
    //}

}
