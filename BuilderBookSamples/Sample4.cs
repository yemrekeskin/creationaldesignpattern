﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // normal kullanım 
    //        var me = new User();

    //        var cloudService = new AwsService();
    //        var dbService = new SqlService();
    //        var autheticationService = new CustomAuthenticationService(cloudService, dbService);

    //        if (autheticationService.IsAuthorized(me))
    //            Console.WriteLine("OK");
    //        else Console.WriteLine("NOK");

    //        // builder ile kullanımı - fluent interface ile build edilmiş bir tasarım
    //        var authService = new CustomAuthenticationServiceBuilder()
    //                                .WithDbService(dbService)
    //                                .WithCloudService(cloudService)
    //                                .Build();

    //        if (authService.IsAuthorized(me))
    //            Console.WriteLine("OK");
    //        else Console.WriteLine("NOK");



    //        Console.ReadLine();
    //    }
    //}



    //public class User { }

    //public interface ICloudProvider { }
    //public class AwsService : ICloudProvider { }
    //public class AzureService : ICloudProvider { }

    //public interface IDatabaseService { }
    //public class OracleService : IDatabaseService { }
    //public class SqlService : IDatabaseService { }

    //// product
    //public class CustomAuthenticationService
    //{
    //    private ICloudProvider _cloudService;
    //    private IDatabaseService _databaseService;

    //    public CustomAuthenticationService(ICloudProvider cloudService, IDatabaseService databaseService)
    //    {
    //        _cloudService = cloudService;
    //        _databaseService = databaseService;
    //    }

    //    public bool IsAuthorized(User user)
    //    {
    //        //Implementation details
    //        return true;

    //    }
    //}

    //// builder
    //public class CustomAuthenticationServiceBuilder
    //{
    //    private ICloudProvider _cloudService;
    //    private IDatabaseService _databaseService;

    //    public CustomAuthenticationServiceBuilder()
    //    {
    //        // custom !
    //        _cloudService = new AwsService();
    //        _databaseService = new SqlService();
    //    }

    //    public CustomAuthenticationServiceBuilder WithAzureService(AzureService azureService)
    //    {
    //        _cloudService = azureService;
    //        return this;
    //    }

    //    public CustomAuthenticationServiceBuilder WithOracleService(OracleService oracleService)
    //    {
    //        _databaseService = oracleService;
    //        return this;
    //    }

    //    // daha generic methodlar
    //    public CustomAuthenticationServiceBuilder WithCloudService(ICloudProvider cloudService)
    //    {
    //        _cloudService = cloudService;
    //        return this;
    //    }

    //    public CustomAuthenticationServiceBuilder WithDbService(IDatabaseService databaseService)
    //    {
    //        _databaseService = databaseService;
    //        return this;
    //    }

    //    public CustomAuthenticationService Build()
    //    {
    //        return new CustomAuthenticationService(_cloudService, _databaseService);
    //    }
    //}
}
