﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BuilderBookSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            // normal kullanım 
            var me = new User();

            var cloudService = new AwsService();
            var dbService = new SqlService();
            var autheticationService = new AuthService(cloudService, dbService);

            if (autheticationService.IsAuthorized(me))
                Console.WriteLine("OK");
            else Console.WriteLine("NOK");

            // builder ile kullanımı - builder pattern 
            var auth = new AuthServiceProvider(new AppAuthServiceBuilder());
            var authProvider = auth.Get();

            if (authProvider.IsAuthorized(me))
                Console.WriteLine("OK");
            else Console.WriteLine("NOK");


            Console.ReadLine();
        }
    }


    
    public class User { }

    public interface ICloudProvider { }
    public class AwsService : ICloudProvider { }
    public class AzureService : ICloudProvider { }

    public interface IDatabaseService { }
    public class OracleService : IDatabaseService { }
    public class SqlService : IDatabaseService { }

    // product
    public class AuthService
    {
        private ICloudProvider _cloudService;
        private IDatabaseService _databaseService;

        public AuthService(ICloudProvider cloudService, IDatabaseService databaseService)
        {
            _cloudService = cloudService;
            _databaseService = databaseService;
        }

        public bool IsAuthorized(User user)
        {
            //Implementation details
            return true;

        }
    }

    // abstract builder
    public abstract class BaseAuthServiceBuilder
    {
        protected ICloudProvider _cloudService;
        protected IDatabaseService _databaseService;

        public abstract void SetCloudService();
        public abstract void SetDatabaseService();

        public virtual AuthService Build()
        {
            var authService = new AuthService(_cloudService, _databaseService);
            return authService;
        }
    }

    // concrete builder
    public class AppAuthServiceBuilder
        : BaseAuthServiceBuilder
    {
        public override void SetCloudService()
        {
            _cloudService = new AwsService();
        }

        public override void SetDatabaseService()
        {
            _databaseService = new SqlService();
        }
    }

    public class ModuleAuthServiceBuilder
        : BaseAuthServiceBuilder
    {
        public override void SetCloudService()
        {
            _cloudService = new AzureService();
        }

        public override void SetDatabaseService()
        {
            _databaseService = new OracleService();
        }
    }

    // directory
    public class AuthServiceProvider
    {
        private BaseAuthServiceBuilder _builder;
        public AuthServiceProvider(BaseAuthServiceBuilder builder)
        {
            _builder = builder;
        }

        public AuthService Get()
        {
            return _builder.Build();
         }
    }
}
