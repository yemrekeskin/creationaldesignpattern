﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BuilderSample4
{
    class Program
    {
        /// <summary>
        /// Inside both requests the same SearchRequest.Construct(string) method is used.
        /// But finally different HttpWebRequest objects are built.
        /// </summary>
        static void Main(string[] args)
        {
            var request1 = new SearchRequest(new HttpWebRequestBuilder());
            var results1 = request1.GetResults("IBM");
            Console.WriteLine(results1);

            var request2 = new SearchRequest(new ProxyHttpWebRequestBuilder("localhost:80"));
            var results2 = request2.GetResults("IBM");
            Console.WriteLine(results2);
            
            Console.ReadLine();
        }
    }
       
    // <summary>
    /// Builder
    /// </summary>
    public interface IWebRequestBuilder
    {
        IWebRequestBuilder BuildHost(string host);
        IWebRequestBuilder BuildPort(int port);
        IWebRequestBuilder BuildPath(string path);
        IWebRequestBuilder BuildQuery(string query);
        IWebRequestBuilder BuildScheme(string scheme);
        IWebRequestBuilder BuildTimeout(int timeout);
        WebRequest Build();
    }

    /// <summary>
    /// ConcreteBuilder #1
    /// </summary>
    public class HttpWebRequestBuilder : IWebRequestBuilder
    {
        private string _host;
        private string _path = string.Empty;
        private string _query = string.Empty;
        private string _scheme = "http";
        private int _port = 80;
        private int _timeout = -1;

        public IWebRequestBuilder BuildHost(string host)
        {
            _host = host;
            return this;
        }

        public IWebRequestBuilder BuildPort(int port)
        {
            _port = port;
            return this;
        }

        public IWebRequestBuilder BuildPath(string path)
        {
            _path = path;
            return this;
        }

        public IWebRequestBuilder BuildQuery(string query)
        {
            _query = query;
            return this;
        }

        public IWebRequestBuilder BuildScheme(string scheme)
        {
            _scheme = scheme;
            return this;
        }

        public IWebRequestBuilder BuildTimeout(int timeout)
        {
            _timeout = timeout;
            return this;
        }

        protected virtual void BeforeBuild(HttpWebRequest httpWebRequest)
        {
        }

        public WebRequest Build()
        {
            var uri = _scheme + "://" + _host + ":" + _port + "/" + _path + "?" + _query;
            
            var httpWebRequest = WebRequest.CreateHttp(uri);
            httpWebRequest.Timeout = _timeout;

            BeforeBuild(httpWebRequest);

            return httpWebRequest;
        }
    }

    /// <summary>
    /// ConcreteBuilder #2
    /// </summary>
    public class ProxyHttpWebRequestBuilder : HttpWebRequestBuilder
    {
        private string _proxy = null;

        public ProxyHttpWebRequestBuilder(string proxy)
        {
            _proxy = proxy;
        }

        protected override void BeforeBuild(HttpWebRequest httpWebRequest)
        {
            httpWebRequest.Proxy = new WebProxy(_proxy);
        }
    }

    /// <summary>
    /// Director
    /// </summary>
    public class SearchRequest
    {

        private IWebRequestBuilder _requestBuilder;

        public SearchRequest(IWebRequestBuilder requestBuilder)
        {
            _requestBuilder = requestBuilder;
        }

        public WebRequest Construct(string searchQuery)
        {
            return _requestBuilder
            .BuildHost("ajax.googleapis.com")
            .BuildPort(80)
            .BuildPath("ajax/services/search/web")
            .BuildQuery("v=1.0&q=" + HttpUtility.UrlEncode(searchQuery))
            .BuildScheme("http")
            .BuildTimeout(-1)
            .Build();
        }

        public string GetResults(string searchQuery)
        {
            var request = Construct(searchQuery);
            var resp = request.GetResponse();

            using (StreamReader stream = new StreamReader(resp.GetResponseStream()))
            {
                return stream.ReadToEnd();
            }
        }
    }


}
