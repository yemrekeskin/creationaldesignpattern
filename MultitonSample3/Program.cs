﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultitonSample3
{
    class Program
    {
        static void Main(string[] args)
        {

            //    TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            //    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            //    ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
            //    ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            //    ScriptEngine engine = new ScriptEngineManager().getEngineByName("rhino");

            Connection conn = Connection.Get(Environment.DEV);
            var conn2 = Connection.Get(Environment.DEV);

            var conn3 = Connection.Get(Environment.PREPROD);
            var conn4 = Connection.Get(Environment.PROD);
            var conn5 = Connection.Get(Environment.UAT);
            

            Console.WriteLine(conn.quid);
            Console.WriteLine(conn2.quid);
            Console.WriteLine(conn3.quid);
            Console.WriteLine(conn4.quid);
            Console.WriteLine(conn5.quid);

            Console.ReadLine();
        }
    }

    public enum Environment
    {
        DEV,
        UAT,
        PREPROD,
        PROD
    }

    // Multiton Class
    public class Connection
    {
        private static Dictionary<Environment, Connection> _instances = new Dictionary<Environment, Connection>();
        private static object _lock = new object();

        public Guid quid { get; set; }

        private Connection()
        {
            quid = Guid.NewGuid();
        }

        public static Connection Get(Environment key)
        {
            lock (_lock)
            {
                if (!_instances.ContainsKey(key))
                    _instances.Add(key, new Connection());
            }
            return _instances[key];
        }

    }

}
