﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultitonSample2
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    #region Sample1

    public sealed class Multiton
    {
        static Dictionary<string, Multiton> _instances = new Dictionary<string, Multiton>();
        // burada nesne koleksiyonumuzu tanımlıyoruz
        // nesneleri tanımlamak veya gruplamak için kullanacağımız key tipi 
        // string , int ya da istediğiniz herhangi bir veri tipi olabilir. 

        static object _lock = new object();

        private Multiton()
        {
            // private ctor
            Console.WriteLine("Nesne yaratıldı");
        }

        public static Multiton GetInstance(string key)
        {
            lock (_lock)
            {
                if (!_instances.ContainsKey(key))
                    _instances.Add(key, new Multiton());
            }
            return _instances[key];
        }
    }

    #endregion

    #region Sample2

    //public class Multiton<T> where T : new() // generic multition.
    //{
    //    private static readonly ConcurrentDictionary<object, T> _instances = new ConcurrentDictionary<object, T>();

    //    private Multiton() { }

    //    public static T GetInstance(object key)
    //    {
    //        return _instances.GetOrAdd(key, (k) => new T());
    //    }
    //}

    public class Multiton<T> where T
        : class, new()
    {
        private static readonly ConcurrentDictionary<object, Lazy<T>> _instances = new ConcurrentDictionary<object, Lazy<T>>();

        public static T GetInstance(object key)
        {
            Lazy<T> instance = _instances.GetOrAdd(key, k => new Lazy<T>(() => new T()));
            return instance.Value;
        }

        private Multiton()
        {
        }
    }

    #endregion

    #region Sample3

    public sealed class Card
    {
        internal Card(string key)
        {
            this.Key = key;
        }

        public string Information { get; set; }
        public string Key { get; set; }

    }

    public sealed class Rolodex
    {
        private static Rolodex _rolodex = new Rolodex();

        private Rolodex()
        {
            this.Cards = new Collection<Card>();
        }

        private Collection<Card> Cards { get; set; }

        public static Card Open(string key)
        {
            Card result = null;

            lock (_rolodex)
            {
                result = _rolodex.Cards
                    .Where(x => string.Equals(x.Key, key, StringComparison.Ordinal))
                    .FirstOrDefault();

                if (null == result)
                {
                    result = new Card(key);
                    _rolodex.Cards.Add(result);
                }
            }

            return result;
        }
    }

    #endregion
}
