﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeSample4
{
    class Program
    {
        static void Main(string[] args)
        {
            var scraper = new WebPageScraper("http://www.google.com");
            scraper.ToString();

            //var scraper2 = new WebPageScraper("http://www.google.com");
            //scraper2.ToString();

            var scraper3 = scraper.Clone() as WebPageScraper; 
            scraper3.ToString();


            Console.ReadLine();
        }
    }

    public class WebPageScraper 
        : ICloneable
    {
        private string title;
        private int headerTagCount;
        private string firstHeaderTagContent;

        public WebPageScraper(string url)
        {
            var client = new WebClient();
            var content = client.DownloadString(url);
            Scrape(content);

        }

        private void Scrape(string page)
        {
            title = "Fake title";
            headerTagCount = 2;
            firstHeaderTagContent = "Fake Header Text";
        }

        public override string ToString()
        {
            var content = "Console : " + title + "-" + headerTagCount.ToString() + "-" + firstHeaderTagContent;
            Console.WriteLine(content);
            return content;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
