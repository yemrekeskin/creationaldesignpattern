﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectPoolSample5
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    public class ObjectPool<T>
    {
        private Queue<T> _pool = new Queue<T>();

        private const int _maxObjects = 100;  // Set this to whatever

        public T Get(params object[] parameters)
        {
            T obj;

            if (_pool.Count < 1)
                obj = (T)Activator.CreateInstance(typeof(T), parameters);
            else
                obj = _pool.Dequeue();

            return obj;
        }

        public void Put(T obj)
        {
            if (_pool.Count < _maxObjects)
                _pool.Enqueue(obj);
        }
    }

    //public interface IPoolable
    //{
    //    void Dispose();
    //}


    //public class ObjectPool<T> where T : IPoolable
    //{
    //    private List<T> pool;

    //    public T Get()
    //    {
    //        if (pool.Count > 0)
    //        {
    //            return pool.First();
    //        }
    //        else
    //        {
    //            return new T(); //  <- How to do this properly?
    //        }
    //    }
    //}

    //public class SomeClass : IPoolable
    //{
    //    int id;

    //    public SomeClass(int id)
    //    {
    //        this.id = id;
    //    }

    //    public void Dispose()
    //    {

    //    }
    //}

    //public class OtherClass : IPoolable
    //{
    //    string name;
    //    int id;

    //    public OtherClass(string name, int id)
    //    {
    //        this.name = name;
    //        this.id = id;
    //    }

    //    public void Dispose()
    //    {

    //    }
    //}
}
