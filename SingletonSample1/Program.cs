﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonSample1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tests started...");

            var a0 = SingletonV1.Instance;
            var a1 = SingletonV1.Instance;

            if(a0 == a1)
                Console.WriteLine("Same Object");
            else Console.WriteLine("Different Object");



            Console.ReadLine();
        }
    }


    // First version - not thread-safe
    // Bad code! Do not use!
    public sealed class SingletonV1
    {
        static SingletonV1 instance = null;
        public string MyProperty { get; set; }
        SingletonV1()
        {
            MyProperty = Guid.NewGuid().ToString();
        }

        public static SingletonV1 Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingletonV1();
                }
                return instance;
            }
        }
    }

    // Second version - simple thread-safety
    public sealed class SingletonV2
    {
        static SingletonV2 instance = null;
        static readonly object padlock = new object();

        SingletonV2()
        {
        }

        public static SingletonV2 Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SingletonV2();
                    }
                    return instance;
                }
            }
        }
    }

    // Third version - attempted thread-safety using double-check locking
    // Bad code! Do not use!
    public sealed class SingletonV3
    {
        static SingletonV3 instance = null;
        static readonly object padlock = new object();

        SingletonV3()
        {
        }

        public static SingletonV3 Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new SingletonV3();
                        }
                    }
                }
                return instance;
            }
        }
    }

    // Fourth version - not quite as lazy, but thread-safe without using locks
    public sealed class SingletonV4
    {
        static readonly SingletonV4 instance = new SingletonV4();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static SingletonV4()
        {
        }

        SingletonV4()
        {
        }

        public static SingletonV4 Instance
        {
            get
            {
                return instance;
            }
        }
    }

    // Fifth version - fully lazy instantiation
    public sealed class SingletonV5
    {
        SingletonV5()
        {
        }

        public static SingletonV5 Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly SingletonV5 instance = new SingletonV5();
        }
    }

    // .NET Singleton
    sealed class Singleton6
    {
        private Singleton6() { }
        public static readonly Singleton6 Instance = new Singleton6();
    }


    // Port to C#
    class Singleton7
    {
        public static Singleton7 Instance()
        {
            if (_instance == null)
            {
                lock (typeof(Singleton7))
                {
                    if (_instance == null)
                    {
                        _instance = new Singleton7();
                    }
                }
            }
            return _instance;
        }
        protected Singleton7() { }
        private static volatile Singleton7 _instance = null;
    }
}
