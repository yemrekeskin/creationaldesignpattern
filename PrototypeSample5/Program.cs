﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeSample5
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Product();
            p.MyProperty1 = 5;
            p.MyProperty2 = "test";

            Console.WriteLine(p.MyProperty2);

            var p2 = p.Clone() as Product;

            Console.WriteLine(p.MyProperty2);

            // değişim sonrası
            p.MyProperty2 = "newtest1";
            p2.MyProperty2 = "newtest2";

            Console.WriteLine("Değişim sonrasında");

            Console.WriteLine(p.MyProperty2);
            Console.WriteLine(p2.MyProperty2);

            Console.ReadLine();

        }
    }

    public abstract class ProductPrototype
    {
        public abstract ProductPrototype Clone();        
    }

    public class Product 
        : ProductPrototype
    {
        public int MyProperty1 { get; set; }
        public string MyProperty2 { get; set; }
        
        public override ProductPrototype Clone()
        {
            // or  (Product)MemberwiseClone();
            return MemberwiseClone() as ProductPrototype;
        }
    }

}
