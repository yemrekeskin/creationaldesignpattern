﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ObjectPoolBookSamples
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            ReusablePool pool = ReusablePool.Instance;
//            Reusable re = pool.Acquire();
//            Reusable re2 = pool.Acquire();

//            Console.WriteLine(re.quid);
//            Console.WriteLine(re2.quid);

//            pool.Release(re);
//            pool.Release(re2);

//            Console.WriteLine("Step1");

//            Reusable re3 = pool.Acquire();
//            Reusable re4 = pool.Acquire();

//            Console.WriteLine(re3.quid);
//            Console.WriteLine(re4.quid);

//            pool.Release(re3);
//            pool.Release(re4);

//            Console.WriteLine("Step2");

//            Reusable re5 = pool.Acquire();
//            Console.WriteLine(re5.quid);

//            pool.Release(re5);

//            Console.ReadLine();
//        }
//    }

//    public class Reusable
//    {
//        public Guid quid { get; set; }
//        public Reusable()
//        {
//            quid = Guid.NewGuid();
//        }
//    }

//    public class ReusablePool
//    {
//        // singleton ReusablePool
//        public static readonly ReusablePool Instance = new ReusablePool();
//        private ReusablePool() { }

//        private static List<Reusable> available = new List<Reusable>();
//        private static List<Reusable> inUse = new List<Reusable>();

//        public Reusable Acquire()
//        {
//            if (available.Count != 0)
//            {
//                Reusable po = available[0];
//                inUse.Add(po);
//                available.RemoveAt(0);
//                return po;
//            }
//            else
//            {
//                Reusable po = new Reusable();
//                inUse.Add(po);
//                return po;
//            }
//        }

//        public void Release(Reusable r)
//        {
//            available.Add(r);
//            inUse.Remove(r);
//        }
//    }

//}
