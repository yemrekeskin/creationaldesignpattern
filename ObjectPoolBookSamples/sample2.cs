﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ObjectPoolBookSamples
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            SquarePool pool = SquarePool.Instance;

//            Square sq1 = pool.Acquire();

//            pool.Release(sq1);

//            Console.ReadLine();
//        }
//    }

//    // reusable
//    public interface IShape { }
//    public class Square
//        : IShape
//    {
//        public Guid quid { get; set; }
//        public Square()
//        {
//            this.quid = Guid.NewGuid();
//        }
//    }

//    // reusable pool
//    public interface IPool
//    {
//        Square Acquire();
//        void Release(Square shape);
//        void Clear();
//    }

//    public class SquarePool
//        : Queue<Square>, IPool
//    {
//        public static readonly SquarePool Instance = new SquarePool();
//        private SquarePool() { }

//        public Square Acquire()
//        {
//            return Dequeue();
//        }

//        public void Release(Square square)
//        {
//            base.Enqueue(square);
//        }

//        public new void Clear()
//        {
//            base.Clear();
//        }
//    }


//}
