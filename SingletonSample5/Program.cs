﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonSample5
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.ReadLine();
        }
    }


    public sealed class ApplicationState
    {
        // Singleton Implementation
        private static ApplicationState _instance;
        private static object _lockThis = new object();

        private ApplicationState() { }

        public static ApplicationState GetState()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new ApplicationState();
            }

            return _instance;
        }

        // State Information
        public string LoginId { get; set; }
        public int MaxSize { get; set; }
    }


    #region Sample1

    public enum LogType { Exception, Info, Error, Fatal }

    public interface ILogger
    {
        void Log(LogType type, string msg);
    }

    public sealed class Logger
        : ILogger
    {
        // Generic Lazy sınıfına kendi tipimizi veriyoruz
        private static readonly Lazy<Logger> lazy = new Lazy<Logger>(() => new Logger());

        // lazy deişkeni üzerinden sınıfın instance değerini dönüyoruz
        public static Logger Instance { get { return lazy.Value; } }

        // private veya protected yapıcı method
        private Logger()
        {
            Console.WriteLine("yapıcı method çalıştı");
        }

        #region functional methods

        public void Log(LogType type, string msg)
        {
            Console.WriteLine(type.ToString() + " - " + msg);
        }

        #endregion
    }

    #endregion
}
