﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorySample6
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseFactory[] factories = new BaseFactory[2];

            factories[0] = new  ConcreteProductAFactory();
            factories[1] = new ConcreteProductBFactory();
            
            foreach (BaseFactory factory in factories)
            {
                BaseProduct product = factory.Factory();
                Console.WriteLine("Created {0}",product.GetType().Name);
            }

            Console.ReadLine();
        }
    }

    // Step 1

    abstract class BaseProduct
    {

    }

    class ConcreteProductA
        : BaseProduct
    {
        
    }

    class ConcreteProductB
        : BaseProduct
    {

    }


    // Step 2

    abstract class BaseFactory
    {
        public abstract BaseProduct Factory();
    }

    class ConcreteProductAFactory
        : BaseFactory
    {
        public override BaseProduct Factory()
        {
            return new ConcreteProductA();
        }
    }

    class ConcreteProductBFactory
        : BaseFactory
    {
        public override BaseProduct Factory()
        {
            return new ConcreteProductB();
        }
    }

}
