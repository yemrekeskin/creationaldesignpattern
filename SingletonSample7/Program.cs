﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonSample7
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.ReadLine();
        }
    }

    //public class ReportProcessor
    //{
    //    public void Process()
    //    {
    //        ReportManager.Instance.ExtractReport();
    //    }
    //}

    //public class ReportManager
    //{
    //    public readonly static ReportManager Instance = new ReportManager();

    //    private ReportManager() { }

    //    public void ExtractReport()
    //    {
    //       /* Extracting Report */     
    //    }
    //}

    public interface IReportManager
    {
        void ExtractReport();
    }

    public class ReportManager
        : IReportManager
    {
        public void ExtractReport()
        {
            /* Extracting report */
        }
    }

    public class ReportProcessor
    {
        private readonly IReportManager _manager;
        public ReportProcessor(IReportManager manager)
        {
            this._manager = manager;
        }

        public void Process()
        {
            this._manager.ExtractReport();
        }
    }
}
