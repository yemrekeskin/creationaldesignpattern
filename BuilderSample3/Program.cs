﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderSample3
{
    class Program
    {
        static void Main(string[] args)
        {
            ReportInfo info = new ReportInfo();
            info.HeaderText = "header";
            info.FooterText = "footer";
            info.ContentText = "context";

            BaseReportBuilder divbasedReport = new DivBasedReport(info);
            var reportManager = new ReportManager(divbasedReport);

            var output = reportManager.Build();

            Console.Write(output);

            Console.ReadLine();
        }
    }

    public class ReportInfo
    {
        public string HeaderText { get; set; }
        public string ContentText { get; set; }
        public string FooterText { get; set; }
    }

    public abstract class BaseReportBuilder
    {
        protected ReportInfo info;
        
        public BaseReportBuilder(ReportInfo info)
        {
            this.info = info;
        }

        public string BuildOutput()
        {
            string output = BuildHeader();
            output += BuildContents();
            output += BuildFooter();

            return output;
        }

        public abstract string BuildHeader();
        public abstract string BuildFooter();
        public abstract string BuildContents();
    }

    public class PdfReport : BaseReportBuilder
    {
        public PdfReport(ReportInfo info) 
            : base(info)
        {

        }

        public override string BuildHeader()
        {
            return "<header>" + info.HeaderText + "</header>";
        }

        public override string BuildFooter()
        {
            return "<footer>" + info.FooterText + "</footer>";
        }

        public override string BuildContents()
        {
            return "<content>" + info.ContentText + "</content>";
        }
    }

    public class DivBasedReport : BaseReportBuilder
    {
        public DivBasedReport(ReportInfo info)
            : base(info)
        {

        }

        public override string BuildHeader()
        {
            return "<h1>" + info.HeaderText + "</h1>";
        }

        public override string BuildFooter()
        {
            return "<h1>" + info.FooterText + "</h1>";
        }

        public override string BuildContents()
        {
            return "<div class='content'>" + info.ContentText + "</div>";
        }
    }


    public class ReportManager
    {
        private BaseReportBuilder builder;
        public ReportManager(BaseReportBuilder builder)
        {
            this.builder = builder;
        }

        public string Build()
        {
            return builder.BuildOutput();
        }

    }
}
