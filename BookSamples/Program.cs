﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            Course techCourse = new Course();
            Course englishCourse = new Course();
            Course businessCourse = new Course();
        }
    }

    public class Course
    {

    }


    public sealed class Singleton
    {
        static Singleton instance = null;
        Singleton()
        {
           
        }

        // getter : static property
        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }
    }



    // Sample 1 - not-Thread-Safe

    public class Logger
    {
        private static Logger _instance = null;
        private Logger() { }  // private constructor

        //public static Logger Instance
        //{
        //    get { return _instance ?? (_instance = new Logger()); }
        //}

        public static Logger Instance()
        {
            return _instance ?? (_instance = new Logger());
        }
    }

    // Sample 2 - Thead-Safe

    public class PerformanceCounter
    {
        private static PerformanceCounter _instance = null;
        private static readonly object Obj = new object();

        private PerformanceCounter() { }

        public static PerformanceCounter Instance
        {
            get
            {
                lock (Obj)
                {
                    return _instance ?? (_instance = new PerformanceCounter());
                }
            }
        }
    }

    // Sample 4 - Lazy initialization

    public sealed class AppConfig
    {
        // Generic Lazy sınıfına kendi tipimizi veriyoruz
        private static readonly Lazy<AppConfig> Lazy = new Lazy<AppConfig>(() => new AppConfig());

        // lazy deişkeni üzerinden sınıfın instance değerini dönüyoruz
        public static AppConfig Instance { get { return Lazy.Value; } }

        // private veya protected yapıcı method
        private AppConfig() { }
    }


    // Sample 4

    public sealed class StringService
    {
        private StringService() { }
        public static readonly StringService Instance = new StringService();

        // StringService için gerekli diğer methodlar
        public override string ToString()
        {
            var formattedString = "newStringValue";

            return formattedString;
        }
    }
}
