using System;
using System.Runtime.Serialization;

[Serializable]
public class TestSingleton
        : ISerializable
{
    private static TestSingleton _instance = null;

    // yapıcı methodun erişim belirleyicisi 'protected' 
    // private da kullanabilirdi.
    protected TestSingleton()
    {
        Console.WriteLine("Nesne oluşturuluyor...");
    }

    public static TestSingleton Instance()
    {
        // Bu kod thread-safe bir kod değil
        if (_instance == null)
            _instance = new TestSingleton();

        return _instance;
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        //info.SetType(typeof(SingletonSerializationHelper));
    }
}

/*[Serializable]
public class Singleton 
    : ISerializable
{
    private static readonly Singleton instance = new Singleton();

    // Private constructor to prevent external instantiation
    private Singleton()
    { }

    public static Singleton Instance
    {
        get { return instance; }
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.SetType(typeof(SingletonSerializationHelper));
    }
}*/

[Serializable]
public class SingletonSerializationHelper 
    : IObjectReference
{
    public object GetRealObject(StreamingContext context)
    {
        return TestSingleton.Instance();
    }
}
