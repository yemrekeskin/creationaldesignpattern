// .NET Singleton
sealed class Singleton2 
{
    private Singleton2() {}
    public static readonly Singleton2 Instance = new Singleton2();
}


// Port to C#
class Singleton3 
{
    public static Singleton3 Instance() {
        if (_instance == null) {
            lock (typeof(Singleton3)) {
                if (_instance == null) {
                    _instance = new Singleton3();
                }
            }
        }
        return _instance;      
    }
    protected Singleton3() {}
    private static volatile Singleton3 _instance = null;
}
