﻿using System;
using System.Collections.Generic;

namespace SingletonSamples
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.ReadLine();
        }
    }

    //#region Sample1

    ///// <summary>
    ///// SAMPLE 1
    ///// </summary>
    //class Singleton
    //{
    //    private static Singleton _instance = null;

    //    // yapıcı methodun erişim belirleyicisi 'protected' 
    //    // private da kullanabilirdi.
    //    protected Singleton()
    //    {
    //        Console.WriteLine("Nesne oluşturuluyor...");
    //    }

    //    public static Singleton Instance()
    //    {
    //        // Bu kod thread-safe bir kod değil
    //        if (_instance == null)
    //            _instance = new Singleton();

    //        return _instance;
    //    }
    //}

    //#endregion

    //#region Sample2

    //public class AppConfig
    //{
    //    private AppConfig()
    //    {
    //        Console.WriteLine("nesne oluşturuldu.");
    //    }

    //    public static AppConfig Instance
    //    {
    //        get
    //        {
    //            // nesne yaratma işlemi nested tip üstleniyor
    //            return AppConfigCreator.instance;
    //        }
    //    }

    //    // nested bir tipin içerisindeki statik bir nesne asıl tipimizin nesnesini oluşturur
    //    // ve bu değişken aracılııyla nesneye erişiriz
    //    private class AppConfigCreator
    //    {
    //        static AppConfigCreator()
    //        {
    //        }

    //        internal static readonly AppConfig instance = new AppConfig();
    //    }

    //    #region functional properties

    //    public string CustomerServiceAddress
    //    {
    //        get { return "http://www.webservicex.net/stockquote.asmx"; }
    //    }

    //    public bool isProd
    //    {
    //        get { return false; }
    //    }

    //    public string ForeignPaymentInFilePath
    //    {
    //        get { return @"C:\PAYMENTS\FOREIGN\IN\"; }
    //    }

    //    #endregion
    //}

    //#endregion

    //#region Sample3

    //public class TestClass
    //{
    //    public override string ToString()
    //    {
    //        return "Test4";
    //    }
    //}

    //public class Singleton<T>
    //    where T : class, new()
    //{
    //    Singleton()
    //    {

    //    }

    //    class SingletonCreator
    //    {
    //        static SingletonCreator() { }
    //        internal static readonly T instance = new T();
    //    }

    //    public static T UniqueInstanse
    //    {
    //        get { return SingletonCreator.instance; }
    //    }
    //}

    //#endregion

    //#region Sample4

    //class SingletonSample
    //     : SingletonBase<SingletonSample>
    //{
    //    public string Something { get; set; }

    //    // !
    //    private SingletonSample()
    //    {
    //        Console.WriteLine("Test");
    //    }

    //}

    //public abstract class SingletonBase<T>
    //    where T : class
    //{
    //    private static readonly Lazy<T> _Instance = new Lazy<T>(() => CreateInstanceOfT());

    //    public static T Instance { get { return _Instance.Value; } }

    //    private static T CreateInstanceOfT()
    //    {
    //        return Activator.CreateInstance(typeof(T), true) as T;
    //    }
    //}

    //#endregion

    //#region Sample5

    //// First version - not thread-safe
    //// Bad code! Do not use!
    //public sealed class SingletonV1
    //{
    //    static SingletonV1 instance = null;

    //    SingletonV1()
    //    {
    //    }

    //    public static SingletonV1 Instance
    //    {
    //        get
    //        {
    //            if (instance == null)
    //            {
    //                instance = new SingletonV1();
    //            }
    //            return instance;
    //        }
    //    }
    //}

    //// Second version - simple thread-safety
    //public sealed class SingletonV2
    //{
    //    static SingletonV2 instance = null;
    //    static readonly object padlock = new object();

    //    SingletonV2()
    //    {
    //    }

    //    public static SingletonV2 Instance
    //    {
    //        get
    //        {
    //            lock (padlock)
    //            {
    //                if (instance == null)
    //                {
    //                    instance = new SingletonV2();
    //                }
    //                return instance;
    //            }
    //        }
    //    }
    //}

    //// Third version - attempted thread-safety using double-check locking
    //// Bad code! Do not use!
    //public sealed class SingletonV3
    //{
    //    static SingletonV3 instance = null;
    //    static readonly object padlock = new object();

    //    SingletonV3()
    //    {
    //    }

    //    public static SingletonV3 Instance
    //    {
    //        get
    //        {
    //            if (instance == null)
    //            {
    //                lock (padlock)
    //                {
    //                    if (instance == null)
    //                    {
    //                        instance = new SingletonV3();
    //                    }
    //                }
    //            }
    //            return instance;
    //        }
    //    }
    //}

    //// Fourth version - not quite as lazy, but thread-safe without using locks
    //public sealed class SingletonV4
    //{
    //    static readonly SingletonV4 instance = new SingletonV4();

    //    // Explicit static constructor to tell C# compiler
    //    // not to mark type as beforefieldinit
    //    static SingletonV4()
    //    {
    //    }

    //    SingletonV4()
    //    {
    //    }

    //    public static SingletonV4 Instance
    //    {
    //        get
    //        {
    //            return instance;
    //        }
    //    }
    //}

    //// Fifth version - fully lazy instantiation
    //public sealed class SingletonV5
    //{
    //    SingletonV5()
    //    {
    //    }

    //    public static SingletonV5 Instance
    //    {
    //        get
    //        {
    //            return Nested.instance;
    //        }
    //    }

    //    class Nested
    //    {
    //        // Explicit static constructor to tell C# compiler
    //        // not to mark type as beforefieldinit
    //        static Nested()
    //        {
    //        }

    //        internal static readonly SingletonV5 instance = new SingletonV5();
    //    }
    //}

    //#endregion

    //#region Sample6

    //public interface IStringManager
    //{
    //    string Get(string key);
    //    string Get(string key, params string[] prms);
    //}

    //public abstract class BaseStringManager
    //    : IStringManager
    //{
    //    public abstract string Get(string key);
    //    public abstract string Get(string key, params string[] prms);
    //}

    //public class SM
    //    : BaseStringManager
    //{
    //    private readonly Dictionary<string, string> values = new Dictionary<string, string>();

    //    private SM()
    //    {
    //        values.Add("WarningToUserPassword", "Kullanıcı şifresi hatalı");
    //        values.Add("CompletedToProcess", "{0} tamamladı");
    //        Console.WriteLine("Created");
    //    }

    //    public static SM Instance
    //    {
    //        get
    //        {
    //            return Nested.instance;
    //        }
    //    }

    //    private class Nested
    //    {
    //        static Nested()
    //        {
    //        }

    //        internal static readonly SM instance = new SM();
    //    }

    //    #region functional methods

    //    public override string Get(string key)
    //    {
    //        // normalde veritabanından bu keye karşılık gelen text metni getirir.
    //        return values[key];
    //    }

    //    public override string Get(string key, params string[] prms)
    //    {
    //        return String.Format(Get(key), prms);
    //    }

    //    #endregion

    //}

    //#endregion

    //#region Sample7

    //public enum LogType { Exception, Info, Error, Fatal }

    //public interface ILogger
    //{
    //    void Log(LogType type, string msg);
    //}

    //public sealed class Logger
    //    : ILogger
    //{
    //    // Generic Lazy sınıfına kendi tipimizi veriyoruz
    //    private static readonly Lazy<Logger> lazy = new Lazy<Logger>(() => new Logger());

    //    // lazy deişkeni üzerinden sınıfın instance değerini dönüyoruz
    //    public static Logger Instance { get { return lazy.Value; } }

    //    // private veya protected yapıcı method
    //    private Logger()
    //    {
    //        Console.WriteLine("yapıcı method çalıştı");
    //    }

    //    #region functional methods

    //    public void Log(LogType type, string msg)
    //    {
    //        Console.WriteLine(type.ToString() + " - " + msg);
    //    }

    //    #endregion
    //}

    //#endregion

    #region Sample8

    public class Dock
    {
        private static Dock _doc;

        private static object lockObj = new object();

        private Finder finder;

        private Dock()
        {

        }

        public static Dock Instance()
        {
            if(_doc == null)
            {
                lock(lockObj)
                {
                    if (_doc == null)
                        _doc = new Dock();
                }
            }

            return _doc;
        }

        public void ClickDock()
        {
            Console.WriteLine("Panel clicked...");

            finder = new Finder();

            Console.WriteLine(finder.Title);
        }
    }

    public class Finder
    {
        public string Title { get; set; }

        private WindowSystem _winSys = new WindowSystem();

        public Finder()
        {
            this.Title = this._winSys.WindowsDraw();
        } 
    }

    public class WindowSystem
    {
        public string WindowsDraw()
        {
            return String.Empty;
        }
    }

    #endregion
}
