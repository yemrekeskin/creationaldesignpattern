﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        var factory = new PDFReaderFactory();
    //        PDFReader reader = (PDFReader)factory.GetReader();
    //        reader.Read(@"C:\...");
    //    }
    //}

    //public abstract class BaseDocumentReader
    //{
    //    public abstract bool IsValid();
    //    public abstract void Read(string filePath);
    //}

    //public class PDFReader : BaseDocumentReader
    //{
    //    public override void Read(string filePath) { }
    //    public override bool IsValid() { return true; }
    //}

    //public class WordReader : BaseDocumentReader
    //{
    //    public override void Read(string filePath) { }
    //    public override bool IsValid() { return true; }
    //}

    //public interface IDocumentReaderFactory
    //{
    //    BaseDocumentReader GetReader();
    //}

    //public class PDFReaderFactory
    //    : IDocumentReaderFactory
    //{
    //    public BaseDocumentReader GetReader()
    //    {
    //        return new PDFReader();
    //    }
    //}

    //public class WordReaderFactory
    //    : IDocumentReaderFactory
    //{
    //    public BaseDocumentReader GetReader()
    //    {
    //        return new WordReader();
    //    }
    //}
}
