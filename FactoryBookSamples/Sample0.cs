﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBookSamples
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // normal kullanım ?
    //        PDFReader pdfReader = new PDFReader();
    //        if (pdfReader.IsValid())
    //            pdfReader.Read(@"C:\...");

    //        // factory ile kullanımı
    //        var factory = new DocumentReaderFactory();
    //        BaseDocumentReader reader = (PDFReader)factory.Get("PDF");
    //        if (reader.IsValid())
    //            reader.Read(@"C:\...");
    //    }
    //}

    //public abstract class BaseDocumentReader
    //{
    //    public abstract bool IsValid();
    //    public abstract void Read(string filePath);
    //}

    //public class PDFReader : BaseDocumentReader
    //{
    //    public override void Read(string filePath) { }
    //    public override bool IsValid() { return true; }
    //}

    //public class WordReader : BaseDocumentReader
    //{
    //    public override void Read(string filePath) { }
    //    public override bool IsValid() { return true; }
    //}

    //public class DocumentReaderFactory
    //{
    //    public BaseDocumentReader Get(string readerType)
    //    {
    //        switch (readerType)
    //        {
    //            case "PDF":
    //                return new PDFReader();
    //            case "MsWord":
    //                return new WordReader();
    //            default:
    //                return null;
    //        }
    //    }
    //}
}
