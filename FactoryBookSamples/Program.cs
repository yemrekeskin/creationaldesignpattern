﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBookSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseDocumentReader pdfReader = (PDFReader)DocumentFactory<PDFReader>.Get();
            BaseDocumentReader wordReader = (WordReader)DocumentFactory<WordReader>.Get();
        }
    }

    public abstract class BaseDocumentReader
    {
        public abstract bool IsValid();
        public abstract void Read(string filePath);   
    }

    public class PDFReader : BaseDocumentReader
    {
        public override void Read(string filePath) { }
        public override bool IsValid() { return true; }
    }

    public class WordReader : BaseDocumentReader
    {
        public override void Read(string filePath) { }
        public override bool IsValid() { return true; }
    }

    public class DocumentFactory<T>
        where T : BaseDocumentReader, new()
    {
        private static BaseDocumentReader opr = null;
        public static BaseDocumentReader Get()
        {
            return opr = new T();
        }
    }


}


