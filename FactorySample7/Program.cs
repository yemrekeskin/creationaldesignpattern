﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorySample7
{
    class Program
    {
        static void Main(string[] args)
        {

            IFactory factorya = new ProductAFactory();
            ProductA a = (ProductA)factorya.Get();

            Console.WriteLine("Created {0}", a.GetType().Name);

            IFactory factoryb = new ProductBFactory();
            ProductB b = (ProductB)factoryb.Get();

            Console.WriteLine("Created {0}", b.GetType().Name);

            Console.ReadLine();
        }
    }

    interface IProduct { }

    class ProductA : IProduct { }

    class ProductB : IProduct { }

    interface IFactory
    {
        IProduct Get();    
    }

    class ProductAFactory : IFactory
    {
        public IProduct Get()
        {
            return new ProductA();
        }
    }

    class ProductBFactory : IFactory
    {
        public IProduct Get()
        {
            return new ProductB();
        }
    }
}
